class HumanPlayer
  def initialize(name)
    @name = name
  end

  def get_play
    puts "Where would you like to attack? (row,col)"
    target = gets.chomp.split(",")
    target.map(&:to_i)
  end
end
