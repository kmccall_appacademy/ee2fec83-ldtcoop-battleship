require_relative 'board.rb'
require_relative 'player.rb'

class BattleshipGame

  def initialize(player = HumanPlayer.new, board = Board.new)
    @board = board
    @player = player
    10.times { @board.place_random_ship }
  end

  def attack(pos)
    puts "You sunk a ship!" if @board[pos] == :s
    @board[pos] = :x
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    self.display
    begin
      self.attack(@player.get_play)
    rescue
      puts "Commander, that is not a valid target!"
      retry
    end
  end

  def display
    puts "There are #{self.count} ships remaining."
    display_grid = @board.grid.map do |subarr|
      subarr.map do |el|
        if el == :x
          "X"
        else
          "O"
        end
      end
    end
    display_grid = display_grid.map do |subarr|
      subarr.join(" ")
    end
    puts display_grid
  end

  attr_reader :board, :player

  def self.play
    puts "What is your name, commander?"
    name = gets.chomp
    player = HumanPlayer.new(name)
    game = BattleshipGame.new(player)
    until game.game_over?
      game.play_turn
    end
    puts "Congradulations! You won!"
  end
end

puts "Press ENTER to begin the game."
gets.chomp
play_game = true
while play_game
  BattleshipGame::play
  play_game = replay?
end
puts "Thanks for playing!"

def replay?
  puts "Play again? (y/n)"
  input = gets.chomp
  loop do
    if input.casecmp("Y").zero?
      return true
    elsif input.casecmp("N").zero?
      return false
    else
      puts "Sorry, what was that?"
    end
  end
end
