require 'byebug'
class Board
  def initialize (grid = Board::default_grid)
    @grid = grid
  end

  attr_reader :grid

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def count
    ship_count = 0
    @grid.each do |subarr|
      subarr.each do |el|
        ship_count += 1 if el == :s
      end
    end
    ship_count
  end

  def empty?(pos = nil)
    # not passed position
    if pos.nil?
      empty_board?
    # passed position
    else
      self[pos].nil?
    end
  end

  def full?
    @grid.all? do |subarr|
      subarr.none?(&:nil?)
    end
  end

  def place_random_ship
    if self.full?
      raise "Board is full!"
    else
      loop do
        pos = random_coordinates
        if self.empty?(pos)
          self[pos] = :s
          break
        end
      end
    end
  end

  def won?
    if remaining_ships?
      false
    else
      true
    end
  end

  private

  def empty_board?
    @grid.all? do |subarr|
      subarr.all?(&:nil?)
    end
  end

  def random_coordinates
    r = rand(0..@grid.length - 1)
    c = rand(0..@grid.first.length - 1)
    [r, c]
  end

  def remaining_ships?
    @grid.any? do |subarr|
      subarr.any? { |el| el == :s }
    end
  end

end
